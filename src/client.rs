use serde::{de::DeserializeOwned, Deserialize, Serialize};

const API_VERSION: &str = "v3";

pub struct Client {
    pub api_base_url: reqwest::Url,
    http: reqwest::Client,
}

#[derive(Debug, Serialize)]
pub struct LoginRequest {
    pub username_or_password: String,
    pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct LoginResponse {
    pub jwt: String,
}

impl Client {
    pub fn new(url: reqwest::Url) -> Self {
        let mut url = url.clone();
        url.path_segments_mut().expect("URL can't be base").extend(&["api", API_VERSION]);

        Self {
            api_base_url: url,
            http: reqwest::Client::new(),
        }
    }

    pub fn from_base_url_str(url: &str) -> Result<Self, url::ParseError> {
        Ok(Self::new(reqwest::Url::parse(url)?))
    }

    async fn request<M, R, E>(
        &self,
        method: reqwest::Method,
        endpoint: E,
        form: M,
    ) -> Result<R, reqwest::Error>
    where
        M: Serialize,
        R: DeserializeOwned,
        E: IntoIterator,
        E::Item: AsRef<str>,
    {
        let mut url = self.api_base_url.clone();
        url.path_segments_mut().unwrap().extend(endpoint);

        let rb = self.http
            .request(method, url)
            .json(&form).build().unwrap();
        println!("{:?}", std::str::from_utf8(rb.body().unwrap().as_bytes().unwrap()));
        self.http.execute(rb)
            .await
            .expect("batatas")
            .error_for_status()
            .unwrap()
            .json::<R>()
            .await
    }

    pub async fn login(&self, form: LoginRequest) -> reqwest::Result<LoginResponse> {
        self.request(reqwest::Method::POST, &["user", "login"], form)
            .await
    }
}
