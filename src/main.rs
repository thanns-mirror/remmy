mod client;

use crate::client::*;

#[tokio::main]
pub async fn main() {
    println!("Hello, world!");
    let client = Client::from_base_url_str("https://lemmy.pt").unwrap();
    let login = LoginRequest {
        username_or_password: "foo".into(),
        password: "barBAZb0rk".into(),
    };
    let resp = client.login(login).await.unwrap();
    println!("{}", resp.jwt);
}
