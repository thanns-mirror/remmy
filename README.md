# remmy

A Lemmy API wrapper written in Rust.


> ⚠️ **Note:** This is still in super early stages! 


### Help and collaboration

We are on [sourcehut](https://sourcehut.com) which means GitHub-like issues are not a thing, and
contributing works a bit differently. 

#### Getting help, discussing features and reporting bugs

All discussion, help and initial bug reporting are done on the 
[mailing list](https://lists.sr.ht/~tmpod/remmy).
This enables anyone with an email account (virtually everyone browsing the Internet) to open a
thread without having to register on this particular git remote.
You also can use the tools you like the most when speaking in these threads.
All you have to do is send a __plain text__ email (this is very important) to
[`u.tmpod.remmy@lists.hsr.ht`](mailto:u.tmpod.remmy@lists.sr.ht).
A thread will automatically be created and subscribers will be notified.
Make sure to follow sourcehut's 
[mailing list etiquette](https://man.sr.ht/lists.sr.ht/etiquette.md).

#### Confirmed features and bugs

In case a feature or bug report is accepted, a more formal ticket will be opened in our 
[issue tracker](https://todo.sr.ht/~tmpod/remmy), which behaves more like what you're used to.
This makes the issue tracker be a much cleaner place from the get-go, with just what's needed in it.

#### Contributing code

Contributing code is also done drastically differently from the workflow GitHub popularized, opting
instead to stick with git's built-in email patch system, that is still in use by big projects such
as the Linux kernel and the PostgreSQL database system.

In essence, you prepare and send a patch with git to the 
[mailing list](https://lists.sr.ht/~tmpod/remmy) and it will then be applied by one of the
maintainers and pushed to the `staging` branch.
From time to time, when the moment is right, a group of patches will be merged onto `main`.

Please refer to this handy tutorial for a proper, hands-on explanation on how to submit a patch: 
https://git-send-email.io


### License

This library is licensed under the 
[GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html).
A copy of this license can be found [here](./COPYING.md) and all files are prefixed with a short
notice.


### Contacts

I can be approached via the [mailing list](mailto:u.tmpod.remmy@lists.sr.ht), my [personal email
address](mailto:tmpod@pm.me) or via [Matrix](matrix:u/tmpod:matrix.org).
